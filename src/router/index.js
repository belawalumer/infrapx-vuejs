import Vue from "vue";
import VueRouter from "vue-router";
import GovernmentDashboard from "../views/GovernmentDashboard.vue";
import HomeDashboard from "../views/HomeDashboard.vue";
import Login from "../views/Login.vue";
import User from "../views/users/User.vue";
import Profile from "../views/users/Profile.vue";
import Import from "../views/Import.vue";
import GovernmentBondDetail from "../views/GovernmentBondDetail.vue";
import InfraPXDashboard from "../views/InfraPXDashboard.vue";
import Financial from "../views/Financials.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "*",
    redirect: "/home",
  },
  {
    path: "/",
    name: "home",
    component: HomeDashboard
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/users",
    name: "users",
    component: User,
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile,
  },
  // {
  //   path: "/userroles",
  //   name: "userroles",
  //   component: UserRole,
  // },
  {
    path: "/import",
    name: "import",
    component: Import
  },
  {
    path: "/infrapxdashboard",
    name: "infrapxdashboard",
    component: InfraPXDashboard
  },
  {
    path: "/government",
    name: "dashboard",
    component: GovernmentDashboard
  },
  {
    name: "detail",
    path: "/detail/:id",
    component: GovernmentBondDetail
  },
  {
    path: "/investor",
    name: "Investor",
    component: () => import("../views/InvestorsDashboard.vue")
  },
  {
    path: "/issuance/:id?",
    name: "Issuance",
    component: () => import("../views/Issuance.vue")
  },
  {
    path: "/continueIssuance",
    name: "ContinueIssuance",
    component: () => import("../views/ContinueIssuance.vue")
  },
  {
    path: "/issueBond",
    name: "IssueBond",
    component: () => import("../views/IssueBond.vue")
  },
  {
    path: "/financials",
    name: "Financials",
    component: Financial,
    props: { type: "government" }
  },
  {
    path: "/investorFinancials",
    name: "InvestorFinancials",
    component: Financial,
    props: { type: "investor" }
  },

  {
    path: "/purchaseBonds",
    name: "PurchaseBonds",
    component: () => import("../views/PurchaseBonds.vue")
  }
];

Vue.prototype.$authUser = JSON.parse(localStorage.getItem("userinfo"));

const router = new VueRouter({
  mode: "history",
  routes,
  base: process.env.BASE_URL,
  linkActiveClass: "active"
});

router.beforeEach((to, from, next) => {    
  if (to.meta.auth && !this.$authUser) {
    next("/login")
  }    
  else {
    next()
  }    
})

export default router;
